import React from "react";
import FeaturesAlt from "./../FeaturesAlt";
import "./styles.scss";

function FeaturesSectionAlt(props) {
  return (
    <section className="FeaturesSectionAlt section is-medium">
      <div className="FeaturesSectionAlt__container container">
        <FeaturesAlt
          items={[
            {
              title: "Amazing Features",
              description:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. A arcu cursus vitae congue. Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique."
            },
            {
              title: "Endless Possibilities",
              description:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. A arcu cursus vitae congue. Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique."
            },
            {
              title: "Ease of use",
              description:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. A arcu cursus vitae congue. Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique."
            }
          ]}
        />
      </div>
    </section>
  );
}

export default FeaturesSectionAlt;
