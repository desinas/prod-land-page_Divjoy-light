import React, { useState } from "react";
import "./styles.scss";

function Navbar(props) {
  const [menuOpen, setMenuOpen] = useState(false);
  return (
    <nav className="navbar is-spaced">
      <div className="container">
        <div className="navbar-brand">
          <a className="navbar-item" href="/">
            <img
              src="https://bulma.io/images/bulma-logo.png"
              width={112}
              height={28}
            />
          </a>
        </div>
        <div className={"navbar-menu" + (menuOpen ? " is-active" : "")} />
      </div>
    </nav>
  );
}

export default Navbar;
