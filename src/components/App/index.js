import React from "react";
import Navbar from "./../Navbar";
import MainSection from "./../MainSection";
import ClientsSection from "./../ClientsSection";
import Divider from "./../Divider";
import FeaturesSection from "./../FeaturesSection";
import TestimonialsSection from "./../TestimonialsSection";
import FeaturesSectionAlt from "./../FeaturesSectionAlt";
import PricingSection from "./../PricingSection";
import Footer from "./../Footer";
import "./styles.scss";

function App(props) {
  return (
    <>
      <Navbar />
      <MainSection />
      <ClientsSection />
      <Divider />
      <FeaturesSection />
      <TestimonialsSection />
      <FeaturesSectionAlt />
      <PricingSection />
      <Footer />
    </>
  );
}

export default App;
